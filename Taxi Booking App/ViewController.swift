//
//  ViewController.swift
//  Taxi Booking App
//
//  Created by Ranjeet Sah on 17/08/2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var btnGetStarted: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btnGetStarted.setRounded(radius: 20.0)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func btnGetStartedAction(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "BookingVC") as! BookingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }


}

