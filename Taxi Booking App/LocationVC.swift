//
//  LocationVC.swift
//  Taxi Booking App
//
//  Created by Ranjeet Sah on 17/08/2021.
//

import UIKit
import CoreLocation

class LocationTableViewCell : UITableViewCell {
    @IBOutlet weak var lblLocation: UILabel!
}


class LocationVC: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    
    let locations = ["Khumaltar,Nepal,44600","LaganaKhel,Nepal,44600","Nakhipot,Nepal,44600"]
    var currentLocation = ""
    var lat = 27.7172
    var longi = 85.3240
    var address = ""
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var tvLocations: UITableView!
    @IBOutlet weak var tfDestination: UITextField!
    @IBOutlet weak var tfSource: UITextField!
    @IBOutlet weak var vLocation: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        tvLocations.delegate = self
        tvLocations.dataSource = self
        locationManager.delegate = self
        
        vLocation.layer.borderWidth = 1.0
        vLocation.layer.borderColor = UIColor.systemGray3.cgColor
        

        if CLLocationManager.authorizationStatus() == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        }

            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell") as! LocationTableViewCell
        
        cell.lblLocation.text = locations[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tfDestination.text = locations[indexPath.row]
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations.first?.coordinate.latitude ?? 27.7172)
        print(locations.first?.coordinate.longitude ?? 85.3240)
        
        self.lat = locations.first?.coordinate.latitude ?? 27.7172
        self.longi = locations.first?.coordinate.longitude ?? 85.3240
        
        if let location = locations.first {
            self.getPlace(for: location) { (p) in
                
                let add = p?.name?.appending(",")
                    .appending(p?.subLocality ?? "").appending(",")
                    .appending(p?.locality ?? "") ?? p?.country ?? "Nepal"
                self.address = add
                self.tfSource.text = self.address
            }
        }
       
    }
    
    
    func getPlace(for location: CLLocation,
                      completion: @escaping (CLPlacemark?) -> Void) {
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location) { placemarks, error in
                
                guard error == nil else {
                    print("*** Error in \(#function): \(error!.localizedDescription)")
                    completion(nil)
                    return
                }
                
                guard let placemark = placemarks?[0] else {
                    print("*** Error in \(#function): placemark is nil")
                    completion(nil)
                    return
                }
                
                completion(placemark)
            }

    }
    
    @IBAction func btnSetOnMap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
