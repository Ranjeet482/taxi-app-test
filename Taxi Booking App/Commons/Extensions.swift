//
//  Extensions.swift
//  Taxi Booking App
//
//  Created by Ranjeet Sah on 17/08/2021.
//

import UIKit

extension UIButton {
    
    func setRounded(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
}
