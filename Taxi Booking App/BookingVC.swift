//
//  BookingVC.swift
//  Taxi Booking App
//
//  Created by Ranjeet Sah on 17/08/2021.
//

import UIKit
import MapKit
import CoreLocation

class BookingVC: UIViewController, MKMapViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tfLocation: UITextField!
    @IBOutlet weak var btnLocation: UIButton!
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        btnLocation.layer.borderColor = UIColor.lightGray.cgColor
        btnLocation.layer.borderWidth = 1.0
        btnLocation.setRounded(radius: 5.0)
        
        
        mapView.delegate = self
            mapView.showsUserLocation = true
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest

            // Check for Location Services
            if CLLocationManager.locationServicesEnabled() {
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
            }
        
    }
    
    @IBAction func textFieldTouched(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }

        if currentLocation == nil {
            // Zoom to user location
            if let userLocation = locations.last {
                let viewRegion = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
                mapView.setRegion(viewRegion, animated: false)
            }
        }
    }

}
